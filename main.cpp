#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

int countchar(string fname, char fdchar);
vector <string> readname(string fname, int taxnum);
vector<string> readchar(string fname, int taxnum, int lnum);
int charlen(string fname, int taxnum, int lnum);
string datatype(vector<string> indchar);
void aligncheck(vector<string> indchar, int taxnum);

int main() {
	//input the filename
	string fname;
	cout << "Please input your file: " << endl;
	cin >> fname;
	//read the file
	int lnum;
	int taxnum;
	int lseq;
	int charnum;
	vector<string> indname;
	vector<string> indchar;	
	lnum = countchar(fname, '\n');
	//cout << "lnum:" << lnum << endl;
	taxnum = countchar(fname, '>');
	charnum = charlen(fname, taxnum, lnum);
	indname = readname(fname, taxnum); 
	indchar = readchar(fname, taxnum, lnum);
	string chardata = datatype(indchar); 
	return 0;
}

int countchar(string fname, char fdchar){
	std::ifstream file;
	file.open(fname, ios::out | ios::in);
	int n = 0;	
	if (!file){
		cout << "Can't open the file!" << endl;
		exit(1);
	} else{
		for(string runstring; getline(file, runstring, fdchar);){
			n ++;
		}	
	}
	file.close();
	return n;
}

vector<string> readname(string fname, int taxnum){
	vector<string> indname (taxnum);
	std::ifstream file;
	file.open(fname, ios::out | ios::in);
	int i = 0;
	for(string runstring; getline(file,runstring);){
		if (runstring[0] == '>'){
			runstring.erase(0,1);
			indname.operator[](i) = runstring;
			i++;
		}
	}
	file.close();
	return indname;
}

vector<string> readchar(string fname, int taxnum, int lnum){
	vector<string> indchar (taxnum);
	std::ifstream file;
	file.open(fname, ios::out | ios::in);
	string tempstring;
	int i = 0;
	int n = 0;
	for(string runstring; getline(file,runstring);){
		while (runstring[0] != '>'){
			if(n==0){
				indchar.operator[](n) = indchar.operator[](n) + runstring;
			}else{
				indchar.operator[](n) = indchar.operator[](n-1) + runstring;
			}
			//tempstring = runstring;
			i++;
			if(i/(lnum/taxnum-1)>1 && i%(lnum/taxnum-1)==0){
				n++;
			}
		}
	}
	file.close();
	return indchar;
}

int charlen(string fname, int taxnum, int lnum){
	int charlen;
	int charlen1 = 0;
	int charlen2 = 0;
	std::ifstream file;
	file.open(fname, ios::out | ios::in);
	int divide = lnum/taxnum;
	int remainder = lnum%taxnum;
	int i = 1;
	
	for(string runstring; getline(file,runstring);){
		if(runstring[0] != '>' && divide == 2 && remainder == 0 && i == 1){	
			i ++;
			charlen = runstring.length();
		}
		for( ; runstring[0] != '>' && divide > 2 && i < lnum/taxnum; i++){
			if(i == 1){
				charlen1 = runstring.length();
			}
			if(i == lnum/taxnum - 1){
				charlen2 = runstring.length();
			}
			i++;
			charlen = charlen1*(lnum/taxnum-2)+charlen2; 
		}
	}
	file.close();
	return charlen;
}

string datatype(vector<string> indchar){
	string chardata;
	string runstring = indchar.operator[](0);
	int orilen = runstring.length();
	runstring.erase(remove(runstring.begin(), runstring.end(), 'A'), runstring.end());
	runstring.erase(remove(runstring.begin(), runstring.end(), 'C'), runstring.end());
	runstring.erase(remove(runstring.begin(), runstring.end(), 'G'), runstring.end());
	int replen = runstring.length();
	if (100*(replen/orilen)<40){
		runstring.erase(remove(runstring.begin(), runstring.end(), 'T'), runstring.end());
		int replen = runstring.length();
		if(100*(replen/orilen)<20){
			chardata = "DNA";
		} else{
			chardata = "RNA";
		}	
	} else{
		runstring.erase(remove(runstring.begin(), runstring.end(), '0'), runstring.end());
		runstring.erase(remove(runstring.begin(), runstring.end(), '1'), runstring.end());
		runstring.erase(remove(runstring.begin(), runstring.end(), '2'), runstring.end());
		int replen = runstring.length();
		if(replen == 0){
			chardata = "standard";
		} else{
			chardata = "Protein";
		}
	}
	return chardata;
}

void aligncheck(vector<string> indchar, int taxnum){
	int i = 0;
	for(int i=0; i<=taxnum-1; i++){
		if(indchar.operator[](i).length() == indchar.operator[](i+1).length()){
	
		}else{
			cout << "The fas files must be aligned!";
		}
	}
}

vector<string> caulcupatt(vector<string> indchar, int charlen, int taxnum){
	vector<string> patt(charlen);
	vector<string> comppatt;
	int veci = 0;
	int stri = 0;
	int count = 1;
	for(;stri<charlen;stri++){
		for(;veci<taxnum;veci++){
			indchar.operator[](veci)[stri];
		}
	}
	return patt;
	return comppatt;
}

